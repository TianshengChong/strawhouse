#!/bin/sh

#This program will run infinite to check ram space
echo "This will refresh every 2 seconds untill you hit ctrl+c"
while :
do
free > memory.txt
echo -e "\t total \t\t used \t\t free"
awk '/[0-9]+/ {print $1"\t" $2/1024 "MB\t" $3/1024"MB\t"$4/1024"MB"}' memory.txt
sleep 2
done
exit 0
